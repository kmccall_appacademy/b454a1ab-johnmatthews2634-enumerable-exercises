require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) {|acc, el| acc + el}
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? {|str| str.include?(substring)}
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  letters = string.split.join.chars
  new_arr = letters.reduce([]) do |acc, el|
    if string.count(el) > 1
      acc << el
    else
      acc
    end
  end
  new_arr.uniq.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.split.sort_by {|word| word.length}[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ("a".."z").to_a.reject {|lttr| string.include?(lttr)}
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).to_a.select {|year| year.to_s.chars.uniq == year.to_s.chars}

end

def not_repeat_year?(year)
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  not_one_wk_wonders = []
  songs.each_with_index do |song, idx|
    if song == songs[idx + 1]
      not_one_wk_wonders << song
    end
  end
  songs.reject {|song| not_one_wk_wonders.include?(song)}.uniq

end

def no_repeats?(song_name, songs)
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  c_word = ""
  string.delete!("?.,!:;")
  string.split.reduce(nil) do |c_dist, word|
    if word.downcase.include?("c") == false
      next
    end
    if c_dist.nil? || word.downcase.chars.reverse.index("c") < c_dist
      c_word = word
      word.downcase.chars.reverse.index("c")
    else
      c_dist
    end
  end
  c_word

end

def c_distance(word)
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  result_arr = []
  arr.each_with_index do |num, idx|
    x = 1
    if num == arr[idx + x]
      starting_idx = idx
      until arr[idx + x] != num
        ending_idx = idx + x
        x += 1
      end
      if result_arr.flatten.include?(ending_idx) || result_arr.flatten.include?(starting_idx)
        next
      end
      result_arr << [starting_idx, ending_idx]
    end
  end
  result_arr

end
